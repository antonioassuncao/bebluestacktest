﻿using System;

namespace BeblueTests.Stacks.Array
{
    public class Stack : IStack
    {
        private int[] Integers;
        private int CurrentStackIndex;

        public Stack()
        {
            Integers = new int[1];
            CurrentStackIndex = 0;
        }

        private void ResizeArray()
        {
            if ( CurrentStackIndex == 0)
            {
                Integers = new int[1];
            }
            else
            {
                int[] newIntegers = new int[CurrentStackIndex];
                for (int i = 0; i < CurrentStackIndex; i++)
                {
                    newIntegers[i] = Integers[i];
                }
                Integers = newIntegers;
            }            
        }

        public void Push(int number)
        {
            if (CurrentStackIndex == Integers.Length)
            {
                System.Array.Resize(ref Integers, Integers.Length + 1);
            }
            Integers[CurrentStackIndex] = number;

            CurrentStackIndex++;
        }

        public int Pop()
        {
            Validation();

            int value = Integers[--CurrentStackIndex];
            Integers[CurrentStackIndex] = default(int);
            ResizeArray();

            return value;
        }

        public int Min()
        {
            Validation();

            int minor = 0;
            for (int i = 0; i < Integers.Length; i++)
            {
                int integer = Integers[i];
                minor = i == 0 ? integer : (integer < minor ? integer : minor);
            }

            return minor;
        }

        private void Validation()
        {
            if (CurrentStackIndex == 0)
                throw new InvalidOperationException("The stack is empty");
        }
    }
}