﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BeblueTests.Stacks.List
{
    public class Stack : List<int>, IStack
    {
        public void Push(int number)
        {
            base.Add(number);
        }

        public int Pop()
        {
            Validation();
            var lastElementInList = base.Count - 1;
            var itemAtTopOfStack = base[lastElementInList];
            base.RemoveAt(lastElementInList);
            return itemAtTopOfStack;
        }

        public int Min()
        {
            Validation();
            return base.ToArray().Min();
        }

        private void Validation()
        {
            if (base.Count == 0)
            {
                throw new InvalidOperationException("The stack is empty");
            }
        }
    }
}