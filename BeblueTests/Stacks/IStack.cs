﻿namespace BeblueTests.Stacks
{
    /// <summary>
    /// Represents a simple last-in-first-out (LIFO).
    /// </summary>
    public interface IStack
    {
        /// <summary>
        /// Inserts an integer at the top of the Stack.
        /// </summary>
        /// <param name="number">The integer to push onto the Stack</param>
        void Push(int number);
        /// <summary>
        /// Removes and returns the integer at the top of the Stack.
        /// </summary>
        /// <returns>The integer removed from the top of the Stack.</returns>
        int Pop();
        /// <summary>
        /// Returns the minimum value from Stack.
        /// </summary>
        /// <returns>The minimum value from Stack</returns>
        int Min();
    }
}