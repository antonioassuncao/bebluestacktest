﻿using System;
using System.Linq;

namespace BeblueTests.Stacks.BaseStack
{
    public class Stack : System.Collections.Stack, IStack
    {
        public void Push(int number)
        {
            base.Push(number);
        }

        public int Pop()
        {
            Validation();
            return (int) base.Pop();
        }

        public int Min()
        {
            Validation();
            return (int)base.ToArray().Min();
        }

        private void Validation()
        {
            if (base.Count == 0)
            {
                throw new InvalidOperationException("The stack is empty");
            }
        }
    }
}