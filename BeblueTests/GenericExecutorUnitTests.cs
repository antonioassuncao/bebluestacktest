﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeblueTests
{
    public class GenericExecutorUnitTests<T> where T : Stacks.IStack
    {
        Stacks.IStack stack = null;

        protected Stacks.IStack GetInstance()
        {
            Stacks.IStack type = (T)Activator.CreateInstance(typeof(T));
            Console.WriteLine("type of this stack: "+type.GetType());
            return type;
        }

        [TestMethod]
        public void TestPushAndPop()
        {
            stack = GetInstance();
            stack.Push(10);
            stack.Push(5);
            Assert.AreEqual(stack.Pop(), 5);
            Assert.AreEqual(stack.Pop(), 10);
        }

        [TestMethod]
        public void TestPushRandomNumberAndGetMinNumber()
        {
            stack = GetInstance();
            Random rnd = new Random();
            int i = 0, maxNumber = 1000, minorNumber = 0;
            do
            {
                int randomNumber = rnd.Next(100, 100 * 100);
                minorNumber = i == 0 ? randomNumber : (randomNumber < minorNumber ? randomNumber : minorNumber);

                stack.Push(randomNumber);
            } while (++i < maxNumber);
            Assert.AreEqual(minorNumber, stack.Min());
        }

        [TestMethod]
        public void TestPushAndPopAndMin()
        {
            stack = GetInstance();
            stack.Push(10);
            stack.Push(5);
            stack.Push(200);
            stack.Push(105);
            stack.Push(67);
            stack.Push(29);

            Assert.AreEqual(stack.Pop(), 29);
            Assert.AreEqual(stack.Pop(), 67);

            stack.Push(3);
            Assert.AreEqual(stack.Min(), 3);

            stack.Push(1);
            Assert.AreEqual(stack.Min(), 1);
        }

        [ExpectedException(typeof(InvalidOperationException))]
        [TestMethod]
        public void TestPopInEmptyStack()
        {
            stack = GetInstance();
            stack.Pop();
        }

        [ExpectedException(typeof(InvalidOperationException))]
        [TestMethod]
        public void TestGetMinInEmptyStack()
        {
            stack = GetInstance();
            stack.Min();
        }
    }
}
